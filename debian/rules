#!/usr/bin/make -f
# -*- makefile -*-

# Get full logs in tests
export G_MESSAGES_DEBUG=all

# Ensure tests fail with criticals
#export G_DEBUG=fatal_criticals

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1
export DPKG_GENSYMBOLS_CHECK_LEVEL=4

include /usr/share/dpkg/architecture.mk

include /usr/share/dpkg/buildflags.mk
# see FEATURE AREAS in dpkg-buildflags(1)
export DEB_BUILD_MAINT_OPTIONS = hardening=+all

export QT_SELECT := 5

undefine DBUS_SESSION_BUS_ADDRESS
undefine DBUS_SYSTEM_BUS_ADDRESS

# Add migrations add-on when build on Ubuntu where the original name is available.
DH_MIGRATIONS = $(shell dpkg-vendor --derives-from Ubuntu && echo "--with migrations")

%:
	dh $@ --with click,gir $(DH_MIGRATIONS)

# FIXME: This should say `--derives-from UBports`, but we haven't got time to
# make a dpkg vendor entry yet.
ENABLE_MIRCLIENT = $(shell \
	dpkg-vendor --derives-from Ubuntu \
		&& echo '-DENABLE_MIRCLIENT=ON' \
		|| echo '-DENABLE_MIRCLIENT=OFF')

override_dh_auto_configure:
	dh_auto_configure -- $(ENABLE_MIRCLIENT)

override_dh_click:
	dh_click --name lomiri-app-launch-desktop

override_dh_missing:
	dh_missing --fail-missing

override_dh_installdeb:
	sed -e"s/#MULTIARCH#/$(DEB_HOST_MULTIARCH)/g" \
		debian/lomiri-app-launch.postinst.in \
		> debian/lomiri-app-launch.postinst
	dh_installdeb

# FIXME: see above.
SUBST_VAR = $(shell \
	dpkg-vendor --derives-from Ubuntu \
		&& echo '-Vlibmir1client-dev=libmir1client-dev (>= 0.5)' \
		|| echo '-Vlibmir1client-dev=')

override_dh_gencontrol:
	dh_gencontrol -- "$(SUBST_VAR)"

override_dh_auto_test:
	mkdir -p $(CURDIR)/debian/test-home/.local/share/
	export HOME=$(CURDIR)/debian/test-home \
	    && export XDG_DATA_HOME=$(CURDIR)/debian/test-home/.local/share \
	    && dh_auto_test --no-parallel

override_dh_clean:
	rm -Rfv $(CURDIR)/debian/test-home/
	dh_clean
