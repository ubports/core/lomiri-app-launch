Overview of changes in lomiri-app-launch 0.1.11

  - liblomiri-app-launch/registry-impl.cpp: Fix parallel access to
    _iconFinders.

Overview of changes in lomiri-app-launch 0.1.10

  - tests: Switch to FUNC compare to make sure we can get async values.
  - liblomiri-app-launch: Support cgroups v2 sysfs interface.
  - cmake/UseGObjectIntrospection.cmake: Fix typelib->gir dependency.
  - liblomiri-app-launch: fix .pc dependency of mir1client.
  - CMakeLists.txt: Support building with against Mir1 package.

Overview of changes in lomiri-app-launch 0.1.9

  - desktop-hook: handle localized Icon & SymbolicIcon (@peat-psuwit).

Overview of changes in lomiri-app-launch 0.1.8

  - CMakeLists.txt: Don't use '${}' with variables in if-clauses.

Overview of changes in lomiri-app-launch 0.1.7

  - tests/liblal-test: fail fast when old GDBusConnection lingers
  - session-migration: remove old ubuntu-app-launch cache
  - CMake: Fail build if USE_SYSTEMD is ON but systemd's pkg-config
    file is missing from build env.
  - Add some thread safety to _observer_{add,delete}_app_* 
  - desktop-hook: also cleanup old files created pre-rename
  - Fix bulding with GCC-13

Overview of changes in lomiri-app-launch 0.1.6

  - liblomiri-app-launch/CMakeLists.txt: Use VERSION_MINOR and VERSION_PATCH in
    library SOVERSIONing and rename variables.
  - liblomiri-app-launch/lomiri-app-launch.pc.in: Use PROJECT_VERSION in
    pkg-config file.

Overview of changes in lomiri-app-launch 0.1.5

  - tests/liblal-test: skip testing for "bus connection gone" for now.
  - jobs-systemd: add Conflicts so that Lomiri unit can stop all apps.
  - tests: list-apps: fix expected number of apps with Liber tine enabled.
  - tests: fix libertined startup argument for verbose output.

Overview of changes in lomiri-app-launch 0.1.4

  - Pass APP_DIR to click applications.
  - jobs-systemd: Set unit CollectMode to "inactive-or-failed".
  - utils/systemd-helper-helper.c: Typo fix.
  - liblomiri-app-launch/jobs-systemd.cpp: Typo fix.
  - job-systemd: don't signal sigStopped/sigStarted on systemd reload.
  - application: Add Splash.show property based on ether NoDisplay or
    x-l-Splash-Show.

Overview of changes in lomiri-app-launch 0.1.3

  - Add option to disable mirclient
  - Add proper versioning (using semver)

Overview of changes in lomiri-app-launch 0.1.2

  - jobs-systemd: Set DESKTOP_FILE_HINT environment variable
  - jobs-systemd: Change unit type to "simple"
  - jobs-systemd: Fix crash when closing a Content Hub source
  - fix: lomiri-app-launch-trace.h is C++
  - systemd-helper-helper: correctly set SA_SIGINFO
  - Run untrusted helpers under Apparmor confinement
  - Fix invocation of unprivileged helpers
  - Add missing #include <array> that caused FTBFS with gcc 12
  - Add fallback support for X-Ubuntu-* desktop file keys
  - cruft: Remove lomiri-app-test
  - Fix typo in DBus object path

Overview of changes in lomiri-app-launch 0.0.90

  - Fork from ubuntu-app-launch, full rename to Lomiri context.
